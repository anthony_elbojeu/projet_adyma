from django.apps import AppConfig


class ApplicationAdymaConfig(AppConfig):
    name = 'application_adyma'
