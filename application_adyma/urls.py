from django.urls import path

from . import views



urlpatterns = [

path('inscription_individuel/', views.inscription_individuel, name='inscription_individuel'),
path('diagnostic_psycho-social/', views.diagnostic_psychosocial, name='diagnostic_psychosocial'),
path('diagnostic_physique_RESIDENCE/', views.diagnostic_physique_residence, name='diagnostic_physique_residence'),
path('diagnostic_physique_EHPAD/', views.diagnostic_physique_ehpad, name='diagnostic_physique_ehpad'),
path('fiche_test/', views.fiche_test, name='fiche_test'),
path('creation_groupe/', views.creation_groupe_activite, name='creation_groupe_activite'),
path('feuille_présence/', views.fiche_presence, name='fiche_presence'),
path("feuille_d'activité/", views.fiche_activite, name='fiche_activite'),
path('inscription_structure/', views.inscription_structure, name='inscription_structure'),
path('diagnostic_structurel/', views.diagnostic_structurel, name='diagnostic_structurel'),
path('visu_structure/$', views.structure_view, name='structure_view'),
path('liste_beneficiaire/$', views.coach_liste_beneficiaire_view, name='coach_liste_beneficiaire_view'),






]
# Attention dans les fichiers HTML, dans les formulaires il y a des liens vers les vues par exemple dans 
# storage.html mettre la vue correspondante questions_save.

# Les vues good et wrong reponses ne sont pas connectées car on ne doit pas les afficher. 
# C'est en fonction de la réponse de l'utilisateur que les vues seront affichées.


# La vue du fichier question.html awnser.1  ne faut pas la changer car ça va permettre de savoir si la question est bonne ou non.