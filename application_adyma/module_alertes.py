from django.db.models import Sum

# Alerte Durée des sorties en moyenne (en minutes)
def point_duree_moyenne_sortie_ext_diag_psycho(query):
    nb_point = 0
    alerte =''
    for instance in query:
        if instance.duree_moyenne_sortie_ext > 60:
            nb_point += 1
        elif instance.duree_moyenne_sortie_ext < 30:
            nb_point += 0
        else:
            nb_point += 0.5
        if instance.duree_moyenne_sortie_ext < 20:
            alerte = 'Durée des sorties en moyenne (en minutes) < 20 minutes'
    return nb_point, alerte

# Alerte Nombre de sorties par semaine
def point_nb_sortie_semaine_diag_psycho(query):
    nb_point = 0
    alerte =''
    for instance in query:
        if instance.nb_sortie_semaine > 3:
            nb_point += 1
        elif instance.nb_sortie_semaine < 1:
            nb_point += 0
        else:
            nb_point += 0.5
        if instance.nb_sortie_semaine <= 3:
            alerte = 'Nombre de sorties en moyenne par semaine <= 3'
    return nb_point, alerte

# Alerte Nombre de chutes dans l'année
def point_nb_chute_annee_diag_psycho(query):
    nb_point = 0
    alerte =''
    for instance in query:
        if instance.nb_chute_annee > 3:
            nb_point += 0
        elif instance.nb_chute_annee < 1:
            nb_point += 1
        else:
            nb_point += 0.5
        if instance.nb_chute_annee >= 1:
            alerte = "Chutes dans l'année >= 1"
    return nb_point, alerte

# Alerte Nombre d'hospitalisations dans l'année
def point_nb_hospitalisation_annee_diag_psycho(query):
    nb_point = 0
    alerte =""
    for instance in query:
        if instance.nb_hospitalisation_annee > 3:
            nb_point += 0
        elif instance.nb_hospitalisation_annee < 1:
            nb_point += 1
        else:
            nb_point += 0.5

        if instance.nb_hospitalisation_annee >= 1:
            alerte = "Nombre d'hospitalisations dans l'année >= 1"
        
        
    return nb_point, alerte

# Alerte Durée de marche maximale estimée
def point_duree_marche_maximal_estime_diag_psycho(query):
    nb_point = 0
    alerte =''
    for instance in query:
    # Si canne déabulateur réduire nb point
        if instance.canne_deambulateur == 1:
            if instance.duree_marche_maximal_estime > 60:
                nb_point += 0.5
            elif instance.duree_marche_maximal_estime < 30:
                nb_point += 0
            else:
                nb_point += 0.25
            if instance.duree_marche_maximal_estime < 20:
                alerte = 'Durée des sorties en moyenne (en minutes) < 20'
        else:
            if instance.duree_marche_maximal_estime > 60:
                nb_point += 1
            elif instance.duree_marche_maximal_estime < 30:
                nb_point += 0
            else:
                nb_point += 0.5
            if instance.duree_marche_maximal_estime < 20:
                alerte = 'Durée des sorties en moyenne (en minutes) < 20'
    
    return nb_point, alerte

# Alerte 5 mètres – outils – levers de chaise
def metre_outil_chaise_diag_psycho(query):
    alerte = ''
    for instance in query:
        if instance.metre_outil_lever_chaise == 0:
            alerte = '5 mètres – outils – levers de chaise  NON OK'
    return alerte

# Alerte Capacité à Aller au sol et se relever
def capacite_sol_releve_diag_psycho(query):
    alerte =''
    for instance in query:
        if instance.capacite_aller_sol_marcher == 0:
            alerte = 'Aller au sol et se relever NON OK'
    return alerte



def total_point_diag_psycho(query):
    total_point_liste = []
    try:
        for instance in query:
            capacite_aller_sol_marcher = instance.capacite_aller_sol_marcher
            envie_sortir_capacite_marcher = instance.envie_sortir_capacite_marcher
            envie_retrouver_partenaire_marcher = instance.envie_retrouver_partenaire_marcher
            desequilibre_deplacement_quotidien = instance.desequilibre_deplacement_quotidien
            connaissance_quartier = instance.connaissance_quartier
            alimentation_correcte = instance.alimentation_correcte
            repos_correct = instance.repos_correct

        total_point_liste = [point_duree_moyenne_sortie_ext_diag_psycho(query)[0],
        point_nb_sortie_semaine_diag_psycho(query)[0],
        point_nb_chute_annee_diag_psycho(query)[0],
        point_nb_hospitalisation_annee_diag_psycho(query)[0],
        point_duree_marche_maximal_estime_diag_psycho(query)[0],
        capacite_aller_sol_marcher,
        envie_sortir_capacite_marcher,
        envie_retrouver_partenaire_marcher,
        desequilibre_deplacement_quotidien,
        connaissance_quartier,
        alimentation_correcte,
        repos_correct,
        ]
        return sum(total_point_liste)

    except:

        return sum(total_point_liste)

def liste_alerte_psychosocial(query):
    liste_alerte_fini = []
    try:
        liste_alerte = [
            point_duree_moyenne_sortie_ext_diag_psycho(query)[1],
            point_nb_sortie_semaine_diag_psycho(query)[1],
            point_nb_chute_annee_diag_psycho(query)[1],
            point_nb_hospitalisation_annee_diag_psycho(query)[1],
            capacite_sol_releve_diag_psycho(query),
            metre_outil_chaise_diag_psycho(query),
            point_duree_marche_maximal_estime_diag_psycho(query)[1]
        ]


        for index, instance in enumerate(liste_alerte):
            if instance != "" :
                liste_alerte_fini.append(instance)
        return liste_alerte_fini
    # for index, instance in enumerate(liste_alerte):
    #     if instance == 1 :
    #         del liste_alerte[index]
    except: 
        return liste_alerte_fini

def total_point_diag_ind_residence(query):
    total_point_liste = []
    try:
        for instance in query:
            total_point_liste = [
            instance.lever_chaise,
            instance.debout_immobile,
            instance.tourner_tete,
            instance.main_nuq,
            instance.enr_vertebral,
            instance.marcher_droit,
            instance.releve_pied,
            instance.long_pas,
            instance.regard_horizon,
            instance.coord_bras_jamb,
            instance.alig_vert,
            instance.respiration,
            instance.march_droit_yeu_fermé,
            instance.march_arriere,
            instance.fent_jamb_droit,
            instance.fent_jamb_gauche,
            instance.cap_sol_lever,
            instance.essouff,
            instance.nbe_chute_ann,
            instance.depl_dehors,

            ]
 
        return sum(total_point_liste)

    except:
        return sum(total_point_liste)

def liste_alerte_diag_ind_residence(query):
    liste_alerte_fini = []
    try:
        if total_point_diag_ind_residence(query) <= 12:
            liste_alerte_fini.append("Attention, somme des points <= 12")
        return liste_alerte_fini
    except: 
        return liste_alerte_fini

   
    alertes = []
    return alertes

def total_point_diag_ind_ehpad_physique(query):
    total_point_liste = []
    try:
        for instance in query:
            total_point_liste = [
                instance.mettre_debout,
                instance.equilibre_deplacement,
                instance.souplesse,
                instance.force_musculaire,
                instance.coordination,
                instance.douleurs,
                instance.fatigabilite,
            ]

        return sum(total_point_liste)

    except:
        return sum(total_point_liste)


def total_point_diag_ind_ehpad_psychologique(query):
    total_point_liste = []
    try:
        for instance in query:
            total_point_liste = [
                instance.estime_soi_mobi,
                instance.motiv_envie_mobiliser,
                instance.interet_bouger,
                instance.profil_energie,
            ]

        return sum(total_point_liste)

    except:
        return sum(total_point_liste)

def total_point_ehpad_relationnel(query):
    total_point_liste = []
    try:
        for instance in query:
            total_point_liste = [
                instance.identi_engagement_act,
                instance.communication,
                instance.niveau_attention,
                instance.profil_relationnel,
            ]

        return sum(total_point_liste)

    except:
        return sum(total_point_liste)


def total_point_diag_structurel(query):
    total_point_liste = []
    try:
        for instance in query:
            total_point_liste = [
                instance.adaptation_env_struc_mobi,
                instance.nombre_activite_semaine,
                instance.salle_activite_reservee,
                instance.materiel_activite_physique,
                instance.liens_reguliers_org_ext,
                instance.nb_personnes_ext_reg_etablissement,
                instance.sorties_sejours_org_annee,
                instance.nb_commerces_prox,
                instance.nb_parcs_prox,
                instance.transports_communs_prox
            ]

        return sum(total_point_liste)

    except:
        return sum(total_point_liste)