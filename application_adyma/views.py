from django.shortcuts import render,get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import AuthenticationForm 
from django.db import connection

from django.views.generic import ListView, DetailView, UpdateView



# Pour plus tard le coach
#user_profil = UserProfil.objects.get(user=request.user) https://openclassrooms.com/forum/sujet/django-recuperer-des-donnees-specifiques


# Formulaires
from .forms import *

# Tables de données
from .models import *

# Module alertes
from .module_alertes import *


from django.shortcuts import redirect
from django.db.models import Sum, Avg

# Système de connexions
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required

from django.contrib import messages











@login_required
def homepage_view(request):
    return render(request, 'application_adyma/home.html')

#---------- FORMS ------------#

@login_required
def inscription_individuel(request):
    if request.method == 'POST':
        form = BeneficiaireForm(request.POST) 
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = BeneficiaireForm()
    return render(request, 'application_adyma/inscriptions/inscription_individuel.html', {'form': form})

def diagnostic_psychosocial(request):
    if request.method == 'POST': 
        form = Diagnostic_psychosocialForm(request.POST) 
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = Diagnostic_psychosocialForm()
    return render(request, 'application_adyma/diagnostics/diagnostic_psycho-social.html', {'form': form})

def diagnostic_physique_residence(request):
    if request.method == 'POST':
        form = Diagnostic_physique_residenceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = Diagnostic_physique_residenceForm()
    return render(request, 'application_adyma/diagnostics/diagnostic_physique_residence.html', {'form': form})

def diagnostic_physique_ehpad(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = Diagnostic_physique_ehpadForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = Diagnostic_physique_ehpadForm() #créer un formulaire vide
    return render(request, 'application_adyma/diagnostics/diagnostic_physique_ehpad.html', {'form': form})

def fiche_test(request):
    if request.method == 'POST':
        form = Fiche_testForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')  
    else:
        form = Fiche_testForm() 
    return render(request, 'application_adyma/exercices/fiche_test.html', {'form': form})

def creation_groupe_activite(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = GroupeForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = GroupeForm() #créer un formulaire vide
    return render(request, 'application_adyma/exercices/groupe_activite.html', {'form': form})

def fiche_presence(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = Fiche_presenceForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = Fiche_presenceForm() #créer un formulaire vide
    return render(request, 'application_adyma/exercices/fiche_presence.html', {'form': form})

def fiche_activite(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = Fiche_activiteForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = Fiche_activiteForm() #créer un formulaire vide
    return render(request, 'application_adyma/exercices/fiche_activite.html', {'form': form})

def inscription_structure(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = StructureForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = StructureForm() #créer un formulaire vide
    return render(request, 'application_adyma/inscriptions/inscription_structure.html', {'form': form})

def diagnostic_structurel(request):
    if request.method == 'POST': # si utilisateur clique sur enregistrer
        form = Diagnostic_structurelForm(request.POST) #enregistrer les données.
        if form.is_valid():
            form.save()
            return redirect('home')  # redirection arpès validation mais pas forcément besoin.
    else:
        form = Diagnostic_structurelForm() #créer un formulaire vide
    return render(request, 'application_adyma/diagnostics/diagnostic_structurel.html', {'form': form})



def structure_view(request):
    user_id = request.user.id
    #liste_beneficiaire_structure = Beneficiaire.objects.raw(f"SELECT DISTINCT bn.*, gp.* FROM application_adyma_beneficiaire AS bn INNER JOIN application_adyma_groupe_beneficiaire AS gpb ON bn.id= beneficiaire_id INNER JOIN application_adyma_groupe AS gp ON gpb.groupe_id = gp.id INNER JOIN application_adyma_structure AS st ON gp.structure_id = st.id INNER JOIN auth_user AS us ON st.nom_structure_id = us.id WHERE us.id = {user_id} order by nom_prenom")
    #liste_beneficiaire_structure = Beneficiaire.objects.raw(f"SELECT DISTINCT bn.*, gp.* FROM application_adyma_beneficiaire AS bn INNER JOIN application_adyma_groupe_beneficiaire AS gpb ON bn.id= beneficiaire_id INNER JOIN application_adyma_groupe AS gp ON gpb.groupe_id = gp.id INNER JOIN application_adyma_structure AS st ON gp.structure_id = st.id INNER JOIN auth_user AS us ON st.nom_structure_id = us.id WHERE us.id = {user_id} order by nom_prenom")
    
    liste_beneficiaire_structure = Beneficiaire.objects.filter(groupe__structure__nom_structure=user_id)
   
    try:
       
        structure_inscription = Structure.objects.filter(nom_structure=user_id)
        diagnostic_structurel = Diagnostic_structurel.objects.filter(structure__nom_structure=user_id)
        total_point_structurel = total_point_diag_structurel(diagnostic_structurel)
        avg_nb_sortie_semaine = Diagnostic_psychosocial.objects.filter(beneficiaire__groupe__structure__nom_structure=user_id ).distinct().aggregate(nb_moy_sort=Avg('nb_sortie_semaine'))
        avg_duree_moyenne_sortie_ext = Diagnostic_psychosocial.objects.filter(beneficiaire__groupe__structure__nom_structure=user_id ).aggregate(nb_moy_sort=Avg('duree_moyenne_sortie_ext'))

        context = {
        "liste_beneficiaire_structure": liste_beneficiaire_structure,
        "structure_inscription": structure_inscription,
        "diagnostic_structurel": diagnostic_structurel,
        "total_point_structurel": total_point_structurel,
        "avg_nb_sortie_semaine": avg_nb_sortie_semaine,
        "avg_duree_moyenne_sortie_ext" :avg_duree_moyenne_sortie_ext
       
    }
        return render(request, 'application_adyma/vues/structure.html', context)

    except ObjectDoesNotExist:
        context = {
         "liste_beneficiaire_structure": liste_beneficiaire_structure,
        }
        return render(request, 'application_adyma/vues/structure.html', context)

        pass


def coach_liste_beneficiaire_view(request):
    user_id = request.user.id
    liste_beneficiaire_coach = Beneficiaire.objects.filter(groupe__coach__user__pk=user_id)
    
    context = {
        "liste_beneficiaire_coach": liste_beneficiaire_coach,
    }
    return render(request, 'application_adyma/vues/coach_liste_beneficiaire.html', context)

def coach_detail_beneficiaire_view(request, id):
    detail_beneficiaire = get_object_or_404(Beneficiaire, pk=id)

    try:
        detail_beneficiaire_diag_psycho = Diagnostic_psychosocial.objects.filter(pk=id)
        detail_beneficiaire_diag_psycho1 = Diagnostic_psychosocial.objects.filter(beneficiaire=id)
        
        print(detail_beneficiaire_diag_psycho1)
        detail_beneficiaire_diag_ind_res = Diagnostic_physique_residence.objects.filter(pk=id)
        detail_beneficiaire_diag_ind_ehpad = Diagnostic_physique_ehpad.objects.filter(pk=id)
    
        total_point_psycho = total_point_diag_psycho(detail_beneficiaire_diag_psycho)
        total_point_indi_res = total_point_diag_ind_residence(detail_beneficiaire_diag_ind_res)
        total_point_ehpad_physique = total_point_diag_ind_ehpad_physique(detail_beneficiaire_diag_ind_ehpad)
        total_point_ehpad_psychologique = total_point_diag_ind_ehpad_psychologique(detail_beneficiaire_diag_ind_ehpad)

        alertes_psychosocial = liste_alerte_psychosocial(detail_beneficiaire_diag_psycho)
        alertes_diag_ind_residence = liste_alerte_diag_ind_residence(detail_beneficiaire_diag_ind_res)
        
        context = { 
            "detail_beneficiaire": detail_beneficiaire,
            "detail_beneficiaire_diag_psycho": detail_beneficiaire_diag_psycho,
            "detail_beneficiaire_diag_ind_res": detail_beneficiaire_diag_ind_res,
            "detail_beneficiaire_diag_ind_ehpad": detail_beneficiaire_diag_ind_ehpad,
            "total_point_psycho" : total_point_psycho,
            "alertes_psychosocial" : alertes_psychosocial,
            "total_point_indi_res" : total_point_indi_res,
            "alertes_diag_ind_residence" : alertes_diag_ind_residence,
            "total_point_ehpad_physique" : total_point_ehpad_physique,
            "total_point_ehpad_psychologique" : total_point_ehpad_psychologique,
       }
        return render(request,
                    'application_adyma/vues/coach_detail_beneficiaire.html', context=context)
    
    except ObjectDoesNotExist:
        context = {
         "detail_beneficiaire": detail_beneficiaire,
        }
        return render(request, 'application_adyma/vues/coach_detail_beneficiaire.html', context)

        pass
