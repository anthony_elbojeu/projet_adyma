from django.contrib import admin
# Register your models here.
from .models import *
#Afficher mieux la vue coach :

from django.contrib.auth.admin import UserAdmin



#admin.site.register(coach)
# Afficher proprement les colonnes d'un formulaire


@admin.register(Beneficiaire)
class GenericAdmin(admin.ModelAdmin):
    list_display = ('nom_prenom',  'date_inscription',  'date_naissance', 'age', 'groupe')
    list_filter = ('nom_prenom', 'orientation', 'date_inscription', 'groupe')
    search_fields = ('nom_prenom',)

class Diagnostic_psychosocialAdmin(admin.ModelAdmin):
    list_display = ('beneficiaire', 'date_diag')
    list_filter = ('beneficiaire', 'date_diag')

admin.site.register(Diagnostic_psychosocial, Diagnostic_psychosocialAdmin)

class Diagnostic_physique_residenceAdmin(admin.ModelAdmin):
    list_display = ( 'beneficiaire', 'date_diag')
    list_filter = ('beneficiaire', 'date_diag')

admin.site.register(Diagnostic_physique_residence, Diagnostic_physique_residenceAdmin)

class Diagnostic_physique_ehpadAdmin(admin.ModelAdmin):
    list_display = ( 'beneficiaire', 'date_diag')
    list_filter = ('beneficiaire', 'date_diag')
    
admin.site.register(Diagnostic_physique_ehpad, Diagnostic_physique_ehpadAdmin)

class Fiche_testAdmin(admin.ModelAdmin):
    list_display = ( 'relev_chaise', 'Aller_sol')

admin.site.register(Fiche_test, Fiche_testAdmin)


class GroupeAdmin(admin.ModelAdmin):
    list_display = ('nom_groupe','description_groupe', 'structure', 'coach')
    list_filter = ('structure', 'coach')
    
admin.site.register(Groupe, GroupeAdmin)


class CoachAdmin(admin.ModelAdmin):
    list_display = ('user','date_inscription_coach')
#, 'Liste_des_structures'
admin.site.register(Coach, CoachAdmin)

class StructureAdmin(admin.ModelAdmin):
    list_display = ('nom_structure','date_inscription_structure','ville_structure', 'nom_directeur_structure', 'telephone_structure')
    list_filter = ('date_inscription_structure', 'nom_prenom_coach_1')
admin.site.register(Structure, StructureAdmin)

class Diagnostic_structurelAdmin(admin.ModelAdmin):
    list_display = ( 'structure', 'date_diagnostic')

admin.site.register(Diagnostic_structurel, Diagnostic_structurelAdmin)













