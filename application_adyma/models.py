from django.db import models
from django.urls import reverse
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import timezone
from django.contrib.auth.models import User
from datetime import date
from multiselectfield import MultiSelectField
from django.utils.translation import ugettext_lazy as _
from django.db.models import Sum
from django.conf import settings
from multiselectfield import MultiSelectField


class Coach(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, unique=True, on_delete=models.CASCADE, verbose_name = "Nom-Prenom du coach")
    date_inscription_coach = models.DateField("Date de l'inscription(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    telephone_coach = PhoneNumberField(null=True, unique=True, blank=True,  verbose_name="Téléphone du coach", help_text="Format téléphone +33 puis numéro de telephone")
    mail_coach = models.EmailField(max_length=100, unique=True, blank=True, null=True, verbose_name="Email du coach")

    def __str__(self):
        return f"{self.user} "

    class Meta:
        verbose_name = "Coach"
        ordering = ['user']


class Structure(models.Model):
    nom_structure = models.ForeignKey(settings.AUTH_USER_MODEL, unique=True, on_delete=models.CASCADE, verbose_name = "Nom de la structure")
    date_inscription_structure= models.DateField(auto_now_add=False, auto_now=False, default=timezone.now, verbose_name = "Date de l'inscription(jour/mois/annee)")
    adresse_structure = models.TextField(null=True,  blank=True, max_length=200, verbose_name="Adresse")
    ville_structure= models.CharField(null=True, blank=True, max_length=50, verbose_name="Ville")
    telephone_structure = PhoneNumberField(null=True, blank=True, unique=True, verbose_name="Téléphone de la structure", help_text="Format téléphone +33 puis numéro de telephone")
    nombre_residents = models.PositiveIntegerField(null=True,  blank=True, verbose_name="Nombre de résidents", default=0)
    nom_directeur_structure= models.CharField(null=True,  blank=True, max_length=50, verbose_name = "Nom du directeur")
    mail_directeur_structure= models.EmailField(null=True,  blank=True, max_length=100,verbose_name="Email du directeur")
    telephone_directeur_structure = PhoneNumberField(null=True, blank=True, unique=True, verbose_name="Téléphone du directeur", help_text="Format téléphone +33 puis numéro de telephone")
    autres_salaries_referents = models.TextField(null=True,  blank=True, verbose_name="Autres salariés référents")
    nom_prenom_coach_1 = models.ManyToManyField(Coach,null=True,  blank=True, verbose_name="Nom-Prenom des coachs de la structure")

    def __str__(self):
        return f"{self.nom_structure} "

class Activite(models.Model):
    nom_activite = models.CharField(max_length=300, unique=True,  verbose_name="Nom de l'activité", )
    date_activite = models.DateField("Date de la fiche de présence(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    type_activite = models.CharField(max_length=100 , null=True, blank=True,  verbose_name="Theme d'activité (psychologique, cardio..)")
    
    class Meta():
        verbose_name="Fiche d'activité"
        ordering=["groupe"]
        

    def __str__(self):
        return f"{self.groupe} - {str(self.date_activite) -{self.nom_activite} }"



class Groupe(models.Model):

    individuel='individuel'
    collectif='collectif'

    type_groupe =(
        (individuel, _('Individuel')),
        (collectif, _('Collectif')),
    )

    nom_groupe = models.CharField(max_length=100, unique=True, verbose_name="Nom du groupe")
    description_groupe = models.TextField()
    structure = models.ForeignKey(Structure, on_delete=models.CASCADE, verbose_name="La structure qui met en place l'activité")
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE, verbose_name="Nom du coach pour le groupe d'activité")
    type_groupe = models.CharField(null=True,  blank=True, max_length=50, choices=type_groupe, default= "individuel", verbose_name="Type de groupe")
    activite_groupe = models.ManyToManyField(Activite, null=True,  blank=True, verbose_name="Les Activités du groupe")
    @property
    def Liste_des_bénéficiaires(self):
        return ",".join([str(p) for p in self.beneficiaire_groupe.all()])

    class Meta():
        verbose_name="Groupe d'activité"
        ordering=["nom_groupe"]

    def __str__(self):
        return f"{self.nom_groupe}"




class Diagnostic_structurel(models.Model):

    class Adaptation(float, models.Choices):
        Espace_adapte = (1), ("Espace bien adapté")
        Espace_peu_adapate = (0.5), ("Espace peu adapté")
        Espace_pas_adapte = (0), ("Espace pas du tout adapaté")

    class Nombre_activites(float, models.Choices):
        bcp_activite = (1), ("+ de 4 activités")
        moyen_activite = (0.5), ("Entre 2 et 4 activités")
        peu_activite = (0), ("Moins de 2 activités")

    class Salle_reservee(float, models.Choices):
        bcp_salle = (1), ("Salle bien adapatée")
        moyen_salle = (0.5), ("Salle peu adaptée")
        peu_salle = (0), ("Aucune salle réservée")

    class Materiel_activite(float, models.Choices):
        bcp_materiel = (1), ("2 matériels ou +")
        moyen_materiel = (0.5), ("1 matériel")
        peu_materiel = (0), ("Aucun matériel")

    class Liens_reguliers(float, models.Choices):
        bcp_lien = (1), ("2 liens +")
        moyen_lien = (0.5), ("1 lien")
        peu_lien = (0), ("Aucun lien")

    class Personnes_ext(float, models.Choices):
        bcp_personne = (1), ("4 personnes +")
        quelques_personne = (0.5), ("Quelques unes")
        peu_personne = (0), ("Aucune personne")

    class Sejours_organise(float, models.Choices):
        bcp_sejour = (1), ("2 sorties/séjours ou plus organisés dans l'année")
        quelques_sejour = (0.5), ("1 sortie/séjour organisé dans l'année")
        peu_sejour = (0), ("Aucune sortie/séjour organisé dans l'année")

    class Commerces_prox(float, models.Choices):
        bcp_commerce = (1), ("3 commerces ou plus à proximité")
        quelques_commerce = (0.5), ("1 ou 2 commerces à proximité")
        peu_commerce = (0), ("Aucun commerce à proximité")
    
    class Parcs_prox(float, models.Choices):
        bcp_parc = (1), ("2 parcs ou plus à proximité")
        quelquesparc = (0.5), ("1 parc à proximité")
        peu_parc = (0), ("Aucune parc à proximité")

    class Transports_prox(float, models.Choices):
        bcp_transport = (1), ("Inférieur à 250m")
        quelquestransport = (0.5), ("Entre 250m et 500m")
        peu_transport = (0), ("Supérieur à 500m")


    structure = models.ForeignKey(Structure, on_delete=models.CASCADE, verbose_name="Nom de la structure")
    date_diagnostic = models.DateField("Date du diagnostic(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    adaptation_env_struc_mobi = models.FloatField(max_length=300, null=True, blank=True, choices=Adaptation.choices, verbose_name="Adaptabilité de l'environnement et de la structure à la mobilité (escaliers, barres d'appuis, luminosité...)")
    nombre_activite_semaine = models.FloatField(max_length=300, null=True, blank=True, choices=Nombre_activites.choices, verbose_name="Nombre d'activité (mémoire, gym, informatique...) par semaine proposées")
    salle_activite_reservee = models.FloatField(max_length=300, null=True, blank=True, choices=Salle_reservee.choices, verbose_name="Salle d'activité réservée pour les activités physiques")
    materiel_activite_physique = models.FloatField(max_length=300, null=True, blank=True, choices=Materiel_activite.choices, verbose_name="Matériel d'activité physique (tapis, machines, espalier)")
    liens_reguliers_org_ext  = models.FloatField(max_length=300, null=True, blank=True, choices=Liens_reguliers.choices, verbose_name="Liens réguliers avec organisation extérieure")
    nb_personnes_ext_reg_etablissement = models.FloatField(max_length=300, null=True, blank=True, choices=Personnes_ext.choices, verbose_name="Nombre de personnes extérieures venant réulièrement dans l'établissement")
    sorties_sejours_org_annee = models.FloatField(max_length=300, null=True, blank=True, choices=Sejours_organise.choices, verbose_name="Sorties et séjours organisés dans l'année")
    nb_commerces_prox = models.FloatField(max_length=300, null=True, blank=True, choices=Commerces_prox.choices, verbose_name="Nombre de commerces de proximité (< 250m)")
    nb_parcs_prox = models.FloatField(max_length=300, null=True, blank=True, choices=Parcs_prox.choices, verbose_name="Nombre de parcs à proximité (< 500m)")
    transports_communs_prox = models.FloatField(max_length=300, null=True, blank=True, choices=Transports_prox.choices, verbose_name="Transports en commun à proximité (en mètres)")

    class Meta:
        verbose_name = "Diagnostic structurel"
        ordering=["structure"]

class Beneficiaire(models.Model):
        
    equilibre = 'equilibre'
    marcher= 'marcher'
    sortir= 'sortir'
    rencontre='rencontre'
    bouger_mentretenir='bouger_mentretenir'
    aucun_interet='aucun_interet'
    peur_tomber='peur_tomber'
    perte_motivation_isolement='perte_motivation_isolement'
    fatigabilite='fatigabilite'
    perte_sens_interet='perte_sens_interet'
    aucun_frein='aucun_frein'
    douleurs='douleurs'
    souplesse='souplesse'
    renforcement_musculaire='renforcement_musculaire'
    gestes_quotidiens='gestes_quotidiens'
    remobilsation_reconditionnement='remobilsation_reconditionnement'
    endurance_gestion_effort='endurance_gestion_effort'
    gestion_stress='gestion_stress'
    coordination='coordination'
    individuel='individuel'
    collectif='collectif'


    interet_projet = [
        (equilibre, _('Equilibre')),
        (marcher, _('Marcher')),
        (sortir, _('Sortir')),
        (rencontre, _('Rencontre')),
        (bouger_mentretenir, _("Bouger - m'entretenir")),
        (aucun_interet, _("Aucun intérêt")),
   ]
    freins_mobilite = [
        (peur_tomber, _('Peur de tomber')),
        (perte_motivation_isolement, _("Perte de motivation lié à l'isolement")),
        (fatigabilite, _('Fatigabilité')),
        (perte_sens_interet, _('Perte de sens et intérêt')),
        (aucun_frein, _("Aucun frein")),
        (douleurs, _("Douleurs")),
   ]

    theme_travail =[
        (souplesse, _('Souplesse')),
        (renforcement_musculaire, _('Renforcement musculaire')),
        (gestes_quotidiens, _('Gestes quotidiens')),
        (remobilsation_reconditionnement, _('Remobilisation et reconditionnement')),
        (endurance_gestion_effort, _('Endurance, gestion, effort')),
        (gestion_stress, _('Souplesse')),
        (coordination, _('Coordination')),
    ]
    type_groupe =(
        (individuel, _('Individuel')),
        (collectif, _('Collectif')),
    )
    
    
    groupe = models.ForeignKey(Groupe, on_delete=models.CASCADE, verbose_name= "Nom du groupe d'activités")
    date_inscription = models.DateField("Date de l'inscription(jour/mois/annee)", auto_now_add=False, auto_now=False, default=timezone.now) #enlever blank=true pour obligation d'ajout. (jai ajouté ça pour que les tests ùmarchent)
    nom_prenom = models.CharField(max_length=300, unique=True,  verbose_name="Nom-Prenom du bénéficiaire" )
    date_naissance = models.DateField(auto_now_add=False, auto_now=False, default=timezone.now, verbose_name="Date de naissance(jour/mois/annee)")     
    DisplayFields =['age']
    appartement = models.PositiveIntegerField(null=True,  blank=True, verbose_name="Numéro appartement")
    telephone = PhoneNumberField(null=True, blank=True, unique=True, verbose_name="Téléphone", help_text="Format téléphone +33 puis numéro de telephone")
    maladie_pathologie = models.TextField(max_length=200, null=True,  blank=True, verbose_name="Maladie/Pathologie")
    nb_activite_structure = models.PositiveSmallIntegerField(null=True,  blank=True, default=0, verbose_name="Nombre d'activités dans la structure")
    interet_proj = MultiSelectField(max_length=300, choices=interet_projet, verbose_name="Intérêt du projet")
    frein_mobi = MultiSelectField(max_length=300, choices=freins_mobilite, verbose_name ="Freins à la mobilité")
    them_trav_souh = MultiSelectField(max_length=300, choices=theme_travail, verbose_name="Thême de travail souhaité")
    orientation = models.CharField(max_length=50, null=True,  blank=True, choices=type_groupe, default= "individuel", verbose_name="Orientation")
    contractualisation = models.BooleanField(max_length=20, verbose_name="Contractualisation", default=True)
    commentaires = models.TextField(blank=True, null=True, verbose_name="Commentaires")

    @property
    def age(self):
        if(self.date_naissance != None):
            age = date.today().year - self.date_naissance.year 
        return age


    class Meta:
        verbose_name = "Bénéficiaire"
        ordering=["nom_prenom"]

    def __str__(self):
        return f"{self.nom_prenom}"
    def get_absolute_url(self):
        return f"/beneficiaires/{self.id}/"



class Diagnostic_psychosocial(models.Model):
    ok = 'ok'
    nonok = 'nonok'

    ok_nonok = [
       (ok, _('Ok')),
       (nonok, _('Non ok')),
   ]
    
    class Capacite(float, models.Choices):
        forte_capacite = (1), ("Oui")
        moyenne_capacite = (0.5), ("Oui avec quelques difficultés")
        faible_capacite = (0), ("Non")

    class Effort(float, models.Choices):
        fort_effort = (1), ("Oui")
        moyen_effort = (0.5), ("Doutes")
        faible_effort = (0), ("Non")

    class Desequilibre(float, models.Choices):
        fort_desequilibre = (0), ("Oui")
        moyen_desequilibre = (0.5), ("Quelques fois")
        faible_desequilibre = (1), ("Non")

    class Quartier(float, models.Choices):
        bcp_commerce = (1), ("Oui")
        quelques_commerce = (0.5), ("Un peu")
        peu_commerce = (0), ("Non")

    class Deambulateur(float, models.Choices):
        avec_canne_deambu = (1), ("Avec canne ou déambulateur")
        sans_canne_deambu = (0), ("Sans canne ou déambulateur")

    class Ok_nonok(float, models.Choices):
        ok = (1), ("Ok")
        non_ok = (0), ("Non ok")

    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, verbose_name="Nom - Prenom du bénéficiaire")
    date_diag = models.DateField("Date du diagnostic Psycho-social(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    instrument_marche = models.BooleanField(max_length=3, verbose_name="Instrument de marche")
    kine = models.BooleanField(max_length=3, null=True,  blank=True, verbose_name="Fait du kiné")
    metre_outil_lever_chaise = models.FloatField( max_length=50, null=True,  blank=True, choices=Ok_nonok.choices, verbose_name="5 mètres – outils – levers de chaise")
    duree_moyenne_sortie_ext = models.FloatField(max_length=100, null=True,  blank=True, verbose_name="Durée moyenne des sorties extérieures (en minutes)")
    nb_sortie_semaine = models.PositiveSmallIntegerField(null=True,  blank=True, verbose_name="Nombre de sorties (en moyenne) par semaine")
    nb_chute_annee = models.PositiveSmallIntegerField(null=True,  blank=True, verbose_name="Nombre de chutes dans l'année")
    nb_hospitalisation_annee = models.PositiveSmallIntegerField(null=True,  blank=True, verbose_name="Nombre d'hospitalisation dans l'année")
    duree_marche_maximal_estime = models.PositiveSmallIntegerField(null=True,  blank=True, verbose_name="Durée de marche maximale estimée (en minutes)")
    canne_deambulateur = models.FloatField(max_length=100,null=True, blank=True, choices=Deambulateur.choices, verbose_name="Canne / déambulateur ?")
    capacite_aller_sol_marcher = models.FloatField(max_length=50, null=True, blank=True, choices=Capacite.choices, default="oui",verbose_name="Capacité à aller au sol et se relever")
    envie_sortir_capacite_marcher = models.FloatField(max_length=50, null=True, blank=True, choices=Effort.choices, default="oui", verbose_name="Envie de sortir et d'entretenir ma capacité à marcher/bouger")
    envie_retrouver_partenaire_marcher = models.FloatField(max_length=50, null=True, blank=True, choices=Effort.choices, default="oui", verbose_name="Envie de retrouver régulièrement un partenaire ou groupe pour marcher/bouger")
    desequilibre_deplacement_quotidien = models.FloatField(max_length=50, null=True, blank=True, choices=Desequilibre.choices, default="oui", verbose_name="Avez-vous des déséquilibres dans vos déplacements au quotidien ?")
    connaissance_quartier = models.FloatField(max_length=50,  null=True, blank=True, choices=Quartier.choices, default="oui", verbose_name="Connaissez-vous bien ce quartier ?")

    alimentation_correcte = models.FloatField(max_length=50, null=True, blank=True, choices=Ok_nonok.choices, default="ok",verbose_name="Alimentation correcte ?")
    repos_correct = models.FloatField(max_length=50, null=True, blank=True, choices=Ok_nonok.choices, default="ok", verbose_name="Repos correct ?")

    class Meta:
        verbose_name="Diagnostic Psychosocial"
        ordering=["beneficiaire"]

    def __str__(self):
        return f"{self.beneficiaire}"


class Diagnostic_physique_residence(models.Model):

    class Mouv(float, models.Choices):
        sans_desequilibre = (1.0), ("Sans déséquilibre ni difficulté")
        quelques_desequilibre = (0.5), ("Quelques déséquilibres et/ou difficultés")
        desequilibre = (0.0), ("Déséquilibres et/ou des difficultés")

    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, verbose_name="Nom - Prenom du bénéficiaire")
    date_diag = models.DateField("Date du diagnostic individuel physique résidence(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    lever_chaise = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Se lever d'une chaise")
    debout_immobile = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Tenir debout immobile")
    tourner_tete = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices,verbose_name="Tourner la tête à gauche et à droite")
    main_nuq = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Passer une main puis l'autre vers la nuque")
    enr_vertebral= models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices,verbose_name="Enroulement vertebral")
    marcher_droit = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Marcher en ligne droite")
    releve_pied = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Relevé du pied / hauteur du pas")
    long_pas = models.FloatField(max_length=50,  null=True, blank=True,choices=Mouv.choices, verbose_name="Longueur du pas")
    regard_horizon = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Regard horizontal")
    coord_bras_jamb = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Coordination bras/jambes")
    alig_vert = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Alignement vertebral")
    respiration = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Respiration")
    march_droit_yeu_fermé = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Marcher en ligne droite yeux fermés")
    march_arriere =models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Marcher en ligne droite en arrière")
    fent_jamb_droit = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Tenir 10 secondes en fente avant jambe droite")
    fent_jamb_gauche = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Tenir 10 secondes en fente avant jambe gauche")
    cap_sol_lever = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Capacité d'aller au sol et se lever")
    essouff = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Essoufflement et/ou rougeur en fin de session")
    nbe_chute_ann = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Nombre de chutes dans l'année")
    depl_dehors = models.FloatField(max_length=50, null=True, blank=True, choices=Mouv.choices, verbose_name="Deplacement dehors au quotidien")
   

    class Meta():
        verbose_name="Diagnostic Individuel Physique - Résidence"
        ordering=["beneficiaire"]

    def __str__(self):
        return f"{self.beneficiaire} - {str(self.date_diag)}"


class Diagnostic_physique_ehpad(models.Model):
    #--- Elements physique ---#
    class Deplacement(float, models.Choices):
        zero = (0), ("Fauteuil roulant sans capacité à le déplacer")
        un = (1), ("Fauteuil roulant - petits déplacements seul")
        deux = (2), ("Déambulateur")
        trois = (3), ("Canne")
        quatre = (4), ("Pas d'instrument d'aide à la marche, quelques difficultés")
        cinq = (5), ("Pas d'instrument d'aide à la marche, marche aisée") 

    class Mettre_debout(float, models.Choices):
        zero = (0), ("Incapacité")
        un = (1), ("Grosse difficulté")
        deux = (2), ("Difficultés et déséquilibres")
        trois = (3), ("Quelques difficultés ou déséquilibres")
        quatre = (4), ("Peu de difficulté ou déséquilibre")
        cinq = (5), ("Pas de difficulté ou déséquilibre") 

    class Equilibre(float, models.Choices):
        zero = (0), ("Très grosses difficultés ou déséquilibres")
        un = (1), ("Grosses difficultés ou déséquilibres")
        deux = (2), ("Difficultés et/ou déséquilibres")
        trois = (3), ("Quelques difficultés et/ou déséquilibres")
        quatre = (4), ("Peu de difficulté ou de déséquilibre")
        cinq = (5), ("Pas de difficulté ou de déséquilibre") 
    
    class Souplesse(float, models.Choices):
        peu_souple = (0), ("Souplesse insuffisante")
        moyen_souple = (0.5), ("Quelques raideurs ou limitations d'amplitude")
        bcp_souple = (1), ("Souplesse suffisante")

    class Force(float, models.Choices):
        peu_force = (0), ("Insuffisante")
        moyen_force = (0.5), ("Quelques manques de forces")
        bcp_force = (1), ("Suffisante")

    class Coordination(float, models.Choices):
        peu_coordiner = (0), ("Insuffisante")
        moyen_coordiner = (0.5), ("Quelques difficultés de coordination")
        bcp_coordiner = (1), ("Suffisante")

    class Douleur(float, models.Choices):
        peu_douleur = (0), ("Douleurs importantes régulières")
        moyen_douleur = (0.5), ("Quelques douleurs régulières")
        bcp_douleur = (1), ("Pas de douleur régulière")

    class Fatigabilite(float, models.Choices):
        peu_fatigue = (0), ("Fatigue régulière et/ou importante")
        moyen_fatigue = (0.5), ("Quelques fatigues importantes et/ou régulières")
        bcp_fatigue = (1), ("Pas de fatigue régulière importante")

    #--- Elements psycho ---#
    class Estime_soi(float, models.Choices):
        peu_estime = (0), ("Ce n'est pas pour moi, je n'en suis pas capable")
        moyen_estime = (2), ("Doutes, questionnements, hésitations..")
        bcp_estime = (4), ("C'est pour moi, j'en suis capable")

    class Motivation(float, models.Choices):
        inactif = (0), ("Inactif")
        passif = (1), ("Passif")
        neutre = (2), ("Neutre")
        suiveur = (3), ("Suiveur")
        actif = (4), ("Actif")
        proactif = (5), ("Proactif") 

    class Projeter_ext(float, models.Choices):
        aucun_interet = (0), ("Auncun intérêt")
        pessimisme = (1), ("Pessimisme, faible intérêt")
        doutes = (2), ("Doutes, questionnements")
        hesitations = (3), ("Hésitations")
        interesse = (4), ("Intéressé")
        enthousiaste = (5), ("Enthousiaste, optimiste") 

    class Profil_energie(float, models.Choices):
        apathique = (0), ("Apathique")
        peu_actif = (2), ("Peu actif")
        equilibre = (5), ("Centré, équilibré")
        # Bien dire que c'est pas possible de mettre deux fois 0 et deux fois 3,5 pour deux valeurs distinque et donc fair eu  choix
        # Agiter normalement c'est 0 et peu actif c'est 2.5 mais j'ai changé.
        eparpille = (2.5), ("Eparpillé")
        agiter = (0.5), ("Grande agitation") 

    #--- Elements sociaux ---#
    class Engagement_activite(float, models.Choices):
        aucune_reconnaissance = (0), ("Aucune reconnaissance ni d'engagement")
        faible_reconnaissance = (1), ("Faible reconnaissance  ou engagement")
        nb_oublis = (2), ("Nombreux oublis")
        quelques_oublis = (3), ("Quelques oublis")
        reconnaissance = (4), ("Reconnaissance et régularité dans l'activité")
        assiduite = (5), ("Assiduité dans l'activité") 

    class Communication(float, models.Choices):
        aucune_com = (0), ("Aucune communication")
        grosse_difficulte_com = (1), ("Grosses difficultés de communication")
        difficulte_com = (2), ("Difficultés pour communiquer")
        quelques_com = (3), ("Quelques difficultés pour communiquer")
        comprehensible_com = (4), ("Communication compréhensible")
        facile_com = (5), ("Communication facile")

    class Niveau_attention(float, models.Choices):
        absent = (0), ("Absent")
        peu_attention = (1), ("Peu d'attention")
        retrait = (2), ("En retrait")
        discret = (3), ("Discret, réservé")
        presence = (4), ("Présence")
        reactivite = (5), ("Spontanéïté et réactivité")

    class Profil_relationnel(float, models.Choices):
        indifference = (0), ("Indifférence")
        tres_conflictuel = (1), ("Très conflictuel")
        conflictuel = (2), ("Conflictuel")
        neutre = (3), ("Neutre")
        cordial = (4), ("Cordial")
        bienveillant = (5), ("Bienveillant, amical") 


    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, verbose_name="Nom - Prenom du bénéficiaire")
    date_diag = models.DateField("Date du diagnostic physique ehpad(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    deplacement_quo = models.FloatField(max_length=50, null=True, blank=True, choices=Deplacement.choices, verbose_name="Deplacements quotidien")
    mettre_debout = models.FloatField(max_length=50, null=True, blank=True, choices=Mettre_debout.choices, verbose_name="Se mettre debout (d'une chaise, fauteuil ou lit)")
    equilibre_deplacement = models.FloatField(max_length=50, null=True, blank=True, choices=Equilibre.choices, verbose_name="Equilibre en déplacement")
    souplesse = models.FloatField(max_length=50, null=True, blank=True, choices=Souplesse.choices, verbose_name="Souplesse")
    force_musculaire = models.FloatField(max_length=50, null=True, blank=True, choices=Force.choices, verbose_name="Force musculaire")
    coordination = models.FloatField(max_length=50, null=True, blank=True, choices=Coordination.choices, verbose_name="Coordination")
    douleurs = models.FloatField(max_length=50, null=True, blank=True, choices=Douleur.choices, verbose_name="Douleurs")
    fatigabilite = models.FloatField(max_length=50, null=True, blank=True, choices=Fatigabilite.choices, verbose_name="Fatigabilité")
    estime_soi_mobi = models.FloatField(max_length=50, null=True, blank=True, choices=Estime_soi.choices, verbose_name="Estime de soi et mobilité")
    motiv_envie_mobiliser = models.FloatField(max_length=50, null=True, blank=True, choices=Motivation.choices, verbose_name="Motivation et envie de se mobiliser")
    interet_bouger = models.FloatField(max_length=50, null=True, blank=True, choices=Projeter_ext.choices, verbose_name="Se projeter à l'extérieur, intérêt à bouger et à sortir de sa chambre")
    profil_energie = models.FloatField(max_length=50, null=True, blank=True, choices=Profil_energie.choices, verbose_name="Profil énergétique")
    identi_engagement_act = models.FloatField(max_length=50, null=True, blank=True, choices=Engagement_activite.choices, verbose_name="Identification et engagement dans l'activité")
    communication = models.FloatField(max_length=50, null=True, blank=True, choices=Communication.choices, verbose_name="Communication")
    niveau_attention = models.FloatField(max_length=50, null=True, blank=True, choices=Niveau_attention.choices, verbose_name="Niveau d'attention et d'interactivité")
    profil_relationnel = models.FloatField(max_length=50, null=True, blank=True, choices=Profil_energie.choices, verbose_name="Profil relationnel")

    class Meta():
        verbose_name="Diagnostic Individuel Physique - Ehpad"
        ordering=["beneficiaire"]
    
    def __str__(self):
        return f"{self.beneficiaire} - {str(self.date_diag)}"

class Fiche_test(models.Model):

    class ok_nonok(models.Choices):
        ("ok"), ("ok")
        ("non_ok"), ("non ok")

    beneficiaire = models.ForeignKey(Beneficiaire, on_delete=models.CASCADE, verbose_name="Nom - Prenom du bénéficiaire")
    date_test= models.DateField(auto_now_add=False, auto_now=False, default=timezone.now, verbose_name = "Date du test jour/mois/annee)")
    tug = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Time Up and GO")
    relev_chaise = models.PositiveIntegerField(null=True, blank=True, verbose_name="Relevé de chaise par minute")
    Aller_sol = models.CharField(max_length=50 , null=True, blank=True, choices=ok_nonok.choices, verbose_name="Aller au sol")
    peri_marche = models.PositiveIntegerField(null=True, blank=True, verbose_name="Périmétrage de marche min/heures (facultatif)")
    
    class Meta():
        verbose_name="Fiche"
    
    def __str__(self):
        return f"{self.beneficiaire} - {str(self.date_test)}"

class Fiche_presence(models.Model):
    date_presence = models.DateField("Date de la fiche de présence(jour/mois/annee)", auto_now_add=False, auto_now=False, blank=True, default=timezone.now)
    
    class Meta():
        verbose_name="Fiche de présence"



