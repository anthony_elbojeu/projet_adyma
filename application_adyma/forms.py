
# Importation formulaires
from django import forms
from django.forms import ModelForm
from django.forms import PasswordInput

# Importation table BD
from application_adyma.models import *


# Extensions
from phonenumber_field.formfields import PhoneNumberField
from phonenumber_field.widgets import PhoneNumberPrefixWidget
from validate_email import validate_email


class BeneficiaireForm(forms.ModelForm):
    telephone = PhoneNumberField(
        widget=PhoneNumberPrefixWidget(initial="FR")
    )
    class Meta:
        model = Beneficiaire
        fields = '__all__'
    
    def __init__(self, *args, **kwargs):
        super(BeneficiaireForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'margin_checkbox',
        })


class Diagnostic_psychosocialForm(forms.ModelForm):
    class Meta:
        model = Diagnostic_psychosocial
        fields = '__all__'
    def __init__(self, *args, **kwargs):
        super(Diagnostic_psychosocialForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'margin_checkbox',
        })
        
        
class Diagnostic_physique_residenceForm(forms.ModelForm):
    class Meta:
        model = Diagnostic_physique_residence 
        fields = '__all__'

class Diagnostic_physique_ehpadForm(forms.ModelForm):
    class Meta:
        model = Diagnostic_physique_ehpad
        fields = '__all__'

class Fiche_testForm(forms.ModelForm):
    class Meta:
        model = Fiche_test
        fields = '__all__'


class GroupeForm(forms.ModelForm):
    class Meta:
        # A REMPLIR
        model = Groupe
        fields = '__all__'


class Fiche_presenceForm(forms.ModelForm):
    class Meta:
        # A REMPLIR
        model = Fiche_presence
        fields = '__all__'

class Fiche_activiteForm(forms.ModelForm):
    class Meta:
        # A REMPLIR
        model = Fiche_test
        fields = '__all__'


class StructureForm(forms.ModelForm):
    telephone_structure = PhoneNumberField(
        widget=PhoneNumberPrefixWidget(initial="FR")
    )
    telephone_directeur_structure = PhoneNumberField(
        widget=PhoneNumberPrefixWidget(initial="FR")
    )
    class Meta:
        model = Structure
        fields = '__all__'


class Diagnostic_structurelForm(forms.ModelForm):
    class Meta:
        # A REMPLIR
        model =  Diagnostic_structurel
        fields = '__all__'








        

